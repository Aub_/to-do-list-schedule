﻿using Contracts;
using Domain.Repositories.Abstractions;
using Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services
{
    public class TaskService : ITaskService
    {
        private readonly IRepositoryManager _repositoryManager;
        public TaskService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }

        public List<TaskBO> Add(List<TaskBO> newTasks)
        {
            List<TaskBO> addedTasks = _repositoryManager.TaskRepository.Add(newTasks);
            return addedTasks;
        }

        public bool Delete(List<int> TaskIds)
        {
            bool existCheck = _repositoryManager.TaskRepository.TasksExist(TaskIds);

            if (!existCheck) return false;

            _repositoryManager.TaskRepository.Delete(TaskIds);
            return true;
        }

        public bool DeleteByUserId(int userId)
        {
            List<TaskBO> tasks = GetByUserId(userId);
            if (tasks is null || tasks.Count == 0) return false;

            List<int> taskIds = tasks.Select(t => t.Id).ToList();

            _repositoryManager.TaskRepository.Delete(taskIds); 
            return true;
        }

        public List<TaskBO> FilterUserTasks(TaskFilter filter)
        {
            return _repositoryManager.TaskRepository.FilterUserTasks(filter);
        }

        public List<TaskBO> GetAll()
        {
            return _repositoryManager.TaskRepository.GetAll();
        }

        public TaskBO GetById(int id)
        {
            return _repositoryManager.TaskRepository.GetById(id);
        }

        public List<TaskBO> GetByUserId(int id)
        {
            return _repositoryManager.TaskRepository.GetByUserId(id);
        }

        public List<TaskBO> Update(List<TaskBO> toBeUpdatedTasks)
        {
            List<int> TaskIds = toBeUpdatedTasks.Select(t => t.Id).ToList();

            bool existCheck = _repositoryManager.TaskRepository.TasksExist(TaskIds);

            if (!existCheck) return null;


            return _repositoryManager.TaskRepository.Update(TaskIds,toBeUpdatedTasks);
        }
    }
}
