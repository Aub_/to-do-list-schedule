﻿using Domain.Repositories.Abstractions;
using Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services
{
    public class ServiceManager : IServiceManager
    {
        private readonly Lazy<ITaskService> _lazyTaskService;
        private readonly Lazy<IReminderService> _lazyReminderService;

        public ServiceManager(IRepositoryManager repositoryManager)
        {
            _lazyTaskService = new Lazy<ITaskService>(() => new TaskService(repositoryManager));
            _lazyReminderService = new Lazy<IReminderService>(() => new ReminderService(repositoryManager));
        }

        public ITaskService TaskService => _lazyTaskService.Value;

        public IReminderService ReminderService => _lazyReminderService.Value;
    }
}
