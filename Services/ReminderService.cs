﻿using Contracts;
using Domain.Repositories.Abstractions;
using Services.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services
{
    public class ReminderService : IReminderService
    {
        private readonly IRepositoryManager _repositoryManager;

        public ReminderService(IRepositoryManager repositoryManager)
        {
            _repositoryManager = repositoryManager;
        }

        public List<ReminderBO> Add(List<ReminderBO> newReminders, int TaskId)
        {
            throw new NotImplementedException();
        }

        public void Delete(List<int> toBeDeletedTasks)
        {
            throw new NotImplementedException();
        }

        public void DeleteByTaskId(int taskId)
        {
            throw new NotImplementedException();
        }

        public void DisableBytTaskId(int taskId)
        {
            throw new NotImplementedException();
        }

        public ReminderBO GetById(int Id)
        {
            return _repositoryManager.ReminderRepository.GetById(Id);
        }

        public List<ReminderBO> GetByIds(List<int> Ids)
        {
            return _repositoryManager.ReminderRepository.GetByIds(Ids);
        }

        public List<ReminderBO> GetByTaskID(int TaskId)
        {
            return _repositoryManager.ReminderRepository.GetByTaskID(TaskId);
        }

        public List<ReminderBO> GetOngingTaskReminders(int TaskId)
        {
            return _repositoryManager.ReminderRepository.GetOngingTaskReminders(TaskId);
        }

        public bool RemindersExist(List<int> ids)
        {
            return _repositoryManager.ReminderRepository.RemindersExist(ids);
        }

        public List<ReminderBO> Update(List<int> OriginalRemindersIds, List<ReminderBO> ToBeUpdatedReminders)
        {
            throw new NotImplementedException();
        }
    }
}
