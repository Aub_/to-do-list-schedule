﻿using API.DTOs;
using Contracts;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using System.Collections.Generic;


namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IServiceManager _serviceManager;

        public TasksController(IServiceManager serviceManager)
        {
            _serviceManager = serviceManager;
        }

        [HttpGet]
        public ActionResult<List<TaskDTO>> Get()
        {
            List<TaskBO> Tasks = _serviceManager.TaskService.GetAll();
            List<TaskDTO> returnModels = TaskDTO.MapToDTOList(Tasks);
            return Ok(returnModels);
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDTO> Get(int id)
        {
            TaskBO Task = _serviceManager.TaskService.GetById(id);
            if (Task is null) return NotFound();

            return Ok(TaskDTO.MapToDTO(Task));
        }

        [HttpGet("user/{user_id}")]
        public ActionResult<TaskDTO> GetByUserId(int user_id)
        {
            List<TaskBO> Tasks = _serviceManager.TaskService.GetByUserId(user_id);
            if (Tasks is null) return NotFound();

            return Ok(TaskDTO.MapToDTOList(Tasks));
        }

        [HttpGet("filter")]
        public ActionResult<TaskDTO> FilterUserTasks([FromQuery] TaskFilterDTO filter)
        {
            TaskFilter taskFilter = TaskFilterDTO.MapToBO(filter);
            if (taskFilter is null) return BadRequest("UserId is required");

            List<TaskBO> Tasks = _serviceManager.TaskService.FilterUserTasks(taskFilter);
            if (Tasks is null || Tasks.Count==0) return NotFound();

            return Ok(TaskDTO.MapToDTOList(Tasks));
        }

        [HttpPost]
        public ActionResult<List<TaskDTO>> Post([FromBody] List<TaskForCreationDTO> taskDTOs)
        {
            List<TaskBO> addedTasks = TaskForCreationDTO.MapToBOList(taskDTOs);
            addedTasks = _serviceManager.TaskService.Add(addedTasks);

            return Ok(TaskDTO.MapToDTOList(addedTasks));
        }

        [HttpPut]
        public ActionResult<List<TaskDTO>> Put([FromBody] List<TaskForUpdateDTO> tasks )
        {
 
            List<TaskBO> tasksForUpdate = TaskForUpdateDTO.MapToBOList(tasks);
            tasksForUpdate = _serviceManager.TaskService.Update(tasksForUpdate);

            if (tasksForUpdate is null) return NotFound();

            return Ok(TaskDTO.MapToDTOList(tasksForUpdate));
        }

        [HttpDelete]
        public ActionResult Delete(List<int> tasks)
        {
             bool result = _serviceManager.TaskService.Delete(tasks);
            if (result) return NoContent();
            return NotFound(); 
        }

        [HttpDelete("user/{id}")]
        public ActionResult DeleteByUserId(int id)
        {
            bool result = _serviceManager.TaskService.DeleteByUserId(id);
            if (result) return NoContent();
            return NotFound();
        }
    }
}
