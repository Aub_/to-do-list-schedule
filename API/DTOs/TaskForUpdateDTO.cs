﻿using Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.DTOs
{
    public class TaskForUpdateDTO
    {
        [Range(1, Int32.MaxValue)]
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime Time { get; set; }
        public int? ParentId { get; set; }
        public int? PriorityId { get; set; }
        public int? StateId { get; set; }
        public bool? IsRecurring { get; set; }
        [Required]
        [Range(1, Int32.MaxValue)]
        public int UserId { get; set; }


        public static TaskForUpdateDTO MapToDTO(TaskBO taskBO)
        {
            TaskForUpdateDTO taskForUpdateDTO = new TaskForUpdateDTO();
            taskForUpdateDTO.Name = taskBO.Name;
            taskForUpdateDTO.PriorityId = taskBO.PriorityId ;
            taskForUpdateDTO.Id = taskBO.Id;
            taskForUpdateDTO.UserId = taskBO.UserId ;
            taskForUpdateDTO.StateId = taskBO.StateId ;
            taskForUpdateDTO.ParentId = taskBO.ParentId ;
            taskForUpdateDTO.IsRecurring = taskBO.IsRecurring;
            taskForUpdateDTO.Time = taskBO.Time;

            return taskForUpdateDTO;
        }

        public static List<TaskForUpdateDTO> MapToDTOList(List<TaskBO> taskBOs)
        {
            List<TaskForUpdateDTO> mappedTasks = new List<TaskForUpdateDTO>();

            foreach (TaskBO taskBO in taskBOs)
            {
                mappedTasks.Add(MapToDTO(taskBO));
            }

            return mappedTasks;
        }

        public static TaskBO MapToBO(TaskForUpdateDTO tasksForUpdate)
        {
            TaskBO taskBO = new TaskBO();
            taskBO.Name = tasksForUpdate.Name ;
            taskBO.PriorityId = tasksForUpdate.PriorityId;
            taskBO.Id = tasksForUpdate.Id;
            taskBO.UserId = tasksForUpdate.UserId ;
            taskBO.StateId = tasksForUpdate.StateId ;
            taskBO.ParentId = tasksForUpdate.ParentId ;
            taskBO.Time = tasksForUpdate.Time ;

            taskBO.IsRecurring = tasksForUpdate.IsRecurring ;

            return taskBO;
        }

        public static List<TaskBO> MapToBOList(List<TaskForUpdateDTO> tasks)
        {
            List<TaskBO> mappedTasks = new List<TaskBO>();
            foreach (TaskForUpdateDTO task in tasks)
            {
                mappedTasks.Add(MapToBO(task));
            }

            return mappedTasks;
        }
    }
}
