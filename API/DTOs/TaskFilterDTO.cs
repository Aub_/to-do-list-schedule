﻿using Contracts;
using System;
using System.ComponentModel.DataAnnotations;

namespace API.DTOs
{
    public class TaskFilterDTO
    {
        public TaskFilterDTO()
        {
            this.StartDate = DateTime.Today;
            this.EndDate = DateTime.Today.AddDays(30);
        }

        [Required]
        public int UserID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public string Priority { get; set; }

        public static TaskFilter MapToBO(TaskFilterDTO taskFilterDTO)
        {
            if(taskFilterDTO.UserID <= 0)
                return null;
            TaskFilter taskFilter = new TaskFilter();
            taskFilter.StartDate = taskFilterDTO.StartDate == null? taskFilter.StartDate : taskFilterDTO.StartDate;
            taskFilter.EndDate = taskFilterDTO.EndDate == null ? taskFilter.EndDate : taskFilterDTO.EndDate;
            taskFilter.Status = taskFilterDTO.Status == null? null : taskFilterDTO.Status.Trim().ToLower();
            taskFilter.UserID = taskFilterDTO.UserID;
            taskFilter.Priority = taskFilterDTO.Priority == null? null : taskFilterDTO.Priority.Trim().ToLower();
            return taskFilter;
        }
    }
}
