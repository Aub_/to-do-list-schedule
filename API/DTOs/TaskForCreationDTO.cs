﻿using Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.DTOs
{
    public class TaskForCreationDTO
    {
        public TaskForCreationDTO()
        {
            this.Reminders = new List<ReminderForCreationDTO>();
        }

        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime? Time { get; set; }
        public int? ParentId { get; set; }
        public int? PriorityId { get; set; }
        public int? StateId { get; set; }
        public bool? IsRecurring { get; set; }
        [Required]
        public int? UserId { get; set; }

        public List<ReminderForCreationDTO> Reminders { get; set; }

        public static TaskBO MapToBO(TaskForCreationDTO taskDTO)
        {
            TaskBO taskBO = new TaskBO();
            taskBO.Name = taskDTO.Name;
            taskBO.PriorityId = taskDTO.PriorityId;
            taskBO.UserId = taskDTO.UserId.Value;
            taskBO.StateId = taskDTO.StateId;
            taskBO.ParentId = taskDTO.ParentId;
            taskBO.IsRecurring = taskDTO.IsRecurring;
            taskBO.Time = taskDTO.Time.Value;

            return taskBO;
        }

        public static List<TaskBO> MapToBOList(List<TaskForCreationDTO> tasks)
        {
            List<TaskBO> mappedTasks = new List<TaskBO>();
            foreach (TaskForCreationDTO task in tasks)
            {
                mappedTasks.Add(MapToBO(task));
            }

            return mappedTasks;
        }
    }
}
