﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API.DTOs
{
    public class ReminderForCreationDTO
    {
        public int TaskId { get; set; }
        public string Description { get; set; }
        [Required]
        public DateTime? Time { get; set; }
    }
}
