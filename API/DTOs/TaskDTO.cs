﻿using Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.DTOs
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Time { get; set; }
        public int? ParentId { get; set; }
        public int? PriorityId { get; set; }
        public int? StateId { get; set; }
        public bool? IsRecurring { get; set; }
        public int UserId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }


        public static TaskDTO MapToDTO( TaskBO taskBO)
        {
            TaskDTO taskDTO = new TaskDTO();
            taskDTO.Name = taskBO.Name;
            taskDTO.PriorityId = taskBO.PriorityId;
            taskDTO.Id = taskBO.Id;
            taskDTO.UserId = taskBO.UserId;
            taskDTO.StateId = taskBO.StateId;
            taskDTO.ParentId = taskBO.ParentId;
            taskDTO.CreatedAt = taskBO.CreatedAt;
            taskDTO.UpdatedAt = taskBO.UpdatedAt;
            taskDTO.DeletedAt = taskBO.DeletedAt;
            taskDTO.IsRecurring = taskBO.IsRecurring;
            taskDTO.Time = taskBO.Time;
            return taskDTO;
        }

        public static List<TaskDTO> MapToDTOList( List<TaskBO> taskBOs)
        {
            List<TaskDTO> mappedTasks = new List<TaskDTO>();

            foreach (TaskBO taskBO in taskBOs)
            {
                mappedTasks.Add(MapToDTO(taskBO));
            }

            return mappedTasks;
        }

        public static TaskBO MapToBO( TaskDTO taskDTO)
        {
            TaskBO taskBO = new TaskBO();
            taskBO.Name = taskDTO.Name;
            taskBO.PriorityId = taskDTO.PriorityId;
            taskBO.Id = taskDTO.Id;
            taskBO.UserId = taskDTO.UserId;
            taskBO.StateId = taskDTO.StateId;
            taskBO.ParentId = taskDTO.ParentId;
            taskBO.Time = taskDTO.Time;

            taskBO.IsRecurring = taskDTO.IsRecurring;

            return taskBO;
        }

        public static List<TaskBO> MapToBOList(List<TaskDTO> tasks)
        {
            List<TaskBO> mappedTasks = new List<TaskBO>();
            foreach (TaskDTO task in tasks)
            {
                mappedTasks.Add(MapToBO(task));
            }

            return mappedTasks;
        }
    }
}
