﻿using Contracts;
using Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Text;
namespace Persistence.Mappers
{
    public static class TaskMapper
    {
        public static Task MapToDAO(TaskBO taskBO)
        {
            Task task = new Task();
            task.Name = taskBO.Name;
            task.PriorityId = taskBO.PriorityId;
            task.Id = taskBO.Id;
            task.UserId = taskBO.UserId;
            task.StateId = taskBO.StateId;
            task.ParentId = taskBO.ParentId;
            task.DateAndTime = taskBO.Time;

            task.IsRecurring = taskBO.IsRecurring;

            return task;
        }

        public static List<Task> MapToDAOList(List<TaskBO> taskBOs)
        {
            List<Task> mappedTasks = new List<Task>();

            foreach (TaskBO taskBO in taskBOs)
            {
                mappedTasks.Add(MapToDAO(taskBO));
            }

            return mappedTasks;
        }

        public static TaskBO MapToBO(this Task task)
        {
            TaskBO taskBO = new TaskBO();
            taskBO.Name = task.Name;
            taskBO.PriorityId = task.PriorityId ;
            taskBO.Id = task.Id;
            taskBO.Time = task.DateAndTime;
            taskBO.UserId = task.UserId;
            taskBO.StateId = task.StateId;
            taskBO.ParentId = task.ParentId;
            taskBO.CreatedAt = task.CreatedAt;
            taskBO.UpdatedAt = task.UpdatedAt;
            taskBO.DeletedAt = task.DeletedAt;
            taskBO.IsRecurring = task.IsRecurring;

            return taskBO;
        }

        public static List<TaskBO> MapToBOList(this List<Task> tasks)
        {
            List<TaskBO> mappedTasks = new List<TaskBO>();
            foreach (Task task in tasks)
            {
                mappedTasks.Add(task.MapToBO());
            }

            return mappedTasks;
        }

        public static List<Task> MapChangesForUpdate(List<TaskBO> tasksForUpdate,List<Task> originalTasks)
        {
            foreach(Task originalTask in originalTasks)
            {
                TaskBO taskForUpdate = tasksForUpdate.Find(t => t.Id == originalTask.Id);

                originalTask.Name = taskForUpdate.Name == null ? originalTask.Name : taskForUpdate.Name;
                originalTask.PriorityId = taskForUpdate.PriorityId is null ? originalTask.PriorityId : taskForUpdate.PriorityId;
                originalTask.StateId = taskForUpdate.StateId is null ? originalTask.StateId : taskForUpdate.StateId;
                originalTask.ParentId = taskForUpdate.ParentId is null ? originalTask.ParentId : taskForUpdate.ParentId;
                originalTask.DateAndTime = taskForUpdate.Time == DateTime.MinValue ? originalTask.DateAndTime : taskForUpdate.Time;
                originalTask.IsRecurring = taskForUpdate.IsRecurring == null ? originalTask.IsRecurring : taskForUpdate.IsRecurring;
            }
            return originalTasks;
        }
    }

}
