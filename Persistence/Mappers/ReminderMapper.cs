﻿using Contracts;
using Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Mappers
{
    public class ReminderMapper
    {
        public static Reminder MapToDAO(ReminderBO ReminderBO)
        {
            Reminder reminderDAO = new Reminder();
            reminderDAO.Description = ReminderBO.Description;
            reminderDAO.IsDone = ReminderBO.IsDone;
            reminderDAO.IsDisabled = ReminderBO.IsDisabled;
            reminderDAO.Id = ReminderBO.Id;
            reminderDAO.Time = ReminderBO.Time;
            reminderDAO.TaskId = ReminderBO.TaskId;

            return reminderDAO;
        }

        public static List<Reminder> MapToDAOList(List<ReminderBO> ReminderBOs)
        {
            List<Reminder> mappedReminders = new List<Reminder>();

            foreach (ReminderBO ReminderBO in ReminderBOs)
            {
                mappedReminders.Add(MapToDAO(ReminderBO));
            }

            return mappedReminders;
        }

        public static ReminderBO MapToBO(Reminder ReminderDAO)
        {
            ReminderBO reminderBO = new ReminderBO();
            reminderBO.Description = ReminderDAO.Description;
            reminderBO.IsDone = ReminderDAO.IsDone;
            reminderBO.IsDisabled = ReminderDAO.IsDisabled;
            reminderBO.Id = ReminderDAO.Id;
            reminderBO.Time = ReminderDAO.Time;
            reminderBO.TaskId = ReminderDAO.TaskId;

            return reminderBO;
        }

        public static List<ReminderBO> MapToBOList(List<Reminder> ReminderDAOs)
        {
            List<ReminderBO> mappedReminders = new List<ReminderBO>();

            foreach (Reminder reminderDAO in ReminderDAOs)
            {
                mappedReminders.Add(MapToBO(reminderDAO));
            }

            return mappedReminders;
        }

        public static List<Reminder> MapChangesForUpdate(List<Reminder> originalList,List<ReminderBO> updatedList)
        {
            foreach (Reminder originalReminder in originalList)
            {
                ReminderBO reminderForUpdate = updatedList.Find(t => t.Id == originalReminder.Id);

                originalReminder.IsDisabled =reminderForUpdate.IsDisabled == null? originalReminder.IsDisabled : reminderForUpdate.IsDisabled;
                originalReminder.IsDone = reminderForUpdate.IsDone == null ? originalReminder.IsDone : reminderForUpdate.IsDone;
                originalReminder.Description = reminderForUpdate.Description == null? originalReminder.Description : reminderForUpdate.Description;
                originalReminder.Time = reminderForUpdate.Time <= DateTime.Now ? originalReminder.Time : reminderForUpdate.Time;
            }
            return originalList;
        }
    }
}
