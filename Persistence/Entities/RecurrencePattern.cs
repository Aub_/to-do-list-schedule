﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Persistence.Entities
{
    [Table("recurrence_patterns")]
    public partial class RecurrencePattern
    {
        [Key]
        public int TaskId { get; set; }
        public int SeparationCount { get; set; }
        public int MaxNumOccurences { get; set; }
        public int DayOfWeek { get; set; }
        public int WeekOfMonth { get; set; }
        public int DayOfMonth { get; set; }
        public int MonthOfYear { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedAt { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DeletedAt { get; set; }

        [ForeignKey(nameof(TaskId))]
        [InverseProperty(nameof(Task.Pattern))]
        public virtual Task ParentTask { get; set; }
    }
}
