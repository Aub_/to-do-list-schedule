﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Persistence.Entities
{
    [Table("reminders")]
    public partial class Reminder
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Required]
        public DateTime Time { get; set; }
        public string Description { get; set; }
        [Column("TaskID")]
        public int TaskId { get; set; }
        public bool? IsDisabled { get; set; }
        [Column("isDone")]
        public bool? IsDone { get; set; }

        [ForeignKey(nameof(TaskId))]
        [InverseProperty(nameof(Task.Reminders))]
        public virtual Task ParentTask { get; set; }
    }
}
