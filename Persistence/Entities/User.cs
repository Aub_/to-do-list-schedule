﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Persistence.Entities
{
    [Table("users")]
    public partial class User
    {
        public User()
        {
            Tasks = new HashSet<Task>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Column("DOB", TypeName = "datetime")]
        public DateTime? Dob { get; set; }
        [StringLength(6)]
        public string Gender { get; set; }
        [Required]
        [StringLength(13)]
        public string PhoneNumber { get; set; }
        [Required]
        [StringLength(25)]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string RefreshToken { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RefreshTokenExpiry { get; set; }
        [Column("VToken")]
        [StringLength(100)]
        public string Vtoken { get; set; }
        [Column("VTokenExpiry", TypeName = "datetime")]
        public DateTime? VtokenExpiry { get; set; }
        public string ProfilePicture { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedAt { get; set; }
        public bool? IsVerified { get; set; }
        public bool? IsActive { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? LastActiveDate { get; set; }

        [InverseProperty(nameof(Task.User))]
        public virtual ICollection<Task> Tasks { get; set; }
    }
}
