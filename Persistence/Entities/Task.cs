﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Persistence.Entities
{
    [Table("tasks")]
    public partial class Task
    {
        public Task()
        {
            InverseParent = new HashSet<Task>();
            Reminders = new HashSet<Reminder>();
        }

        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime DateAndTime { get; set; }
        [Column("ParentID")]
        public int? ParentId { get; set; }
        [Column("PriorityID")]
        public int? PriorityId { get; set; }
        [Column("StateID")]
        public int? StateId { get; set; }
        public bool? IsRecurring { get; set; }
        [Column("UserID")]
        public int UserId { get; set; }
        public DateTime? EndDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedAt { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedAt { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DeletedAt { get; set; }

        [ForeignKey(nameof(ParentId))]
        [InverseProperty(nameof(Task.InverseParent))]
        public virtual Task Parent { get; set; }
        [ForeignKey(nameof(PriorityId))]
        [InverseProperty(nameof(TaskPriority.Tasks))]
        public virtual TaskPriority Priority { get; set; }
        [ForeignKey(nameof(StateId))]
        [InverseProperty(nameof(TaskState.Tasks))]
        public virtual TaskState State { get; set; }
        [ForeignKey(nameof(UserId))]
        [InverseProperty("Tasks")]
        public virtual User User { get; set; }
        [InverseProperty(nameof(Task.Parent))]
        public virtual ICollection<Task> InverseParent { get; set; }

        [InverseProperty(nameof(RecurrencePattern.ParentTask))]
        public virtual RecurrencePattern Pattern { get; set; }

        [InverseProperty(nameof(Reminder.ParentTask))]
        public virtual ICollection<Reminder> Reminders { get; set; }
    }
}
