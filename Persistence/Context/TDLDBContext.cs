﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Persistence.Entities;

#nullable disable

namespace Persistence
{
    public partial class TDLDBContext : DbContext
    {
        public TDLDBContext()
        {
        }

        public TDLDBContext(DbContextOptions<TDLDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Reminder> Reminders { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<TaskPriority> TaskPriorities { get; set; }
        public virtual DbSet<TaskState> TaskStates { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-ONKAFLJ;Database=TDLDB;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Reminder>(entity =>
            {
                entity.Property(e => e.IsDisabled).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsDone).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<Task>(entity =>
            {
                entity.Property(e => e.IsRecurring).HasDefaultValueSql("((0))");

                entity.Property(e => e.PriorityId).HasDefaultValueSql("((2))");

                entity.Property(e => e.StateId).HasDefaultValueSql("((2))");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK__tasks__ParentID__2F10007B");

                entity.HasOne(d => d.Priority)
                    .WithMany(p => p.Tasks)
                    .HasForeignKey(d => d.PriorityId)
                    .HasConstraintName("FK__tasks__PriorityI__300424B4");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.Tasks)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK__tasks__StateID__30F848ED");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Tasks)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__tasks__UserID__31EC6D26");
            });

            modelBuilder.Entity<TaskPriority>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();
            });

            modelBuilder.Entity<TaskPriority>().HasData(
                new TaskPriority { Id = 1, Name = "High", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now},
                new TaskPriority { Id = 2, Name = "Medium", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now },
                new TaskPriority { Id = 3, Name = "Low", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now });

            modelBuilder.Entity<TaskState>().HasData(
                new TaskState { Id = 1, Name = "New", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now },
                new TaskState { Id = 2, Name = "Done", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now },
                new TaskState { Id = 3, Name = "Cancelled", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now });


            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.IsActive).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsVerified).HasDefaultValueSql("((0))");
            });



            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        public override int SaveChanges()
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => (
                        e.State == EntityState.Added
                        || e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                entityEntry.Property("UpdatedAt").CurrentValue = DateTime.Now;

                if (entityEntry.State == EntityState.Added)
                {
                    entityEntry.Property("CreatedAt").CurrentValue = DateTime.Now;
                }
            }

            return base.SaveChanges();
        }
    }
}
