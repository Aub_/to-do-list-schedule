﻿using Contracts;
using Domain.Repositories.Abstractions;
using Persistence;
using Persistence.Entities;
using Persistence.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistence.Repositories
{
    public class ReminderRepository : IReminderRepository
    {
        private readonly TDLDBContext _context;

        public ReminderRepository(TDLDBContext context)
        {
            _context = context;
        }

        public List<ReminderBO> Add(List<ReminderBO> newReminders, int TaskId)
        {
            List<Reminder> reminders = ReminderMapper.MapToDAOList(newReminders);

            _context.Reminders.AddRange(reminders);

            _context.SaveChanges();
            return ReminderMapper.MapToBOList(reminders);
        }

        public void Delete(List<int> toBeDeletedTaskIds)
        {
            List<Reminder> reminders = _context.Reminders
               .Where(r => toBeDeletedTaskIds.Contains(r.Id)).ToList();
            _context.Reminders.RemoveRange(reminders);
            _context.SaveChanges();
        }

        public void DeleteByTaskId(int taskId)
        {
            List<Reminder> reminders = _context.Reminders
               .Where(r => r.TaskId == taskId).ToList();
            _context.Reminders.RemoveRange(reminders);
            _context.SaveChanges();
        }

        public void DisableBytTaskId(int taskId)
        {
            List<Reminder> reminders = _context.Reminders
              .Where(r => r.TaskId == taskId && r.IsDone == false).ToList();

            foreach(Reminder reminder in reminders)
            {
                reminder.IsDisabled = true;
            }
            _context.SaveChanges();
        }

        public ReminderBO GetById(int Id)
        {
            Reminder reminder = _context.Reminders.Where(r=>r.Id == Id).FirstOrDefault();
            return ReminderMapper.MapToBO(reminder);
        }

        public List<ReminderBO> GetByIds(List<int> ids)
        {
            List<Reminder> reminders = _context.Reminders.Where(t => ids.Contains(t.Id)).ToList();
            return ReminderMapper.MapToBOList(reminders);
        }

        public List<ReminderBO> GetByTaskID(int TaskId)
        {
            List<Reminder> reminders = _context.Reminders.Where(r => r.TaskId == TaskId).ToList();
            return ReminderMapper.MapToBOList(reminders);
        }

        public List<ReminderBO> GetOngingTaskReminders(int TaskId)
        {
            List<Reminder> reminders = _context.Reminders
                .Where(r => r.IsDisabled != true && r.IsDone != true && r.TaskId == TaskId).ToList();
            return ReminderMapper.MapToBOList(reminders);
        }

        public bool RemindersExist(List<int> ids)
        {
            List<Reminder> reminders = _context.Reminders.Where(t => ids.Contains(t.Id)).ToList();
            return ids.Except(reminders).Count() == 0;
        }

        public List<ReminderBO> Update(List<int> OriginalRemindersIds, List<ReminderBO> ToBeUpdatedReminders)
        {
            List<Reminder> originalReminders = _context.Reminders
                .Where(t => OriginalRemindersIds.Contains(t.Id)).ToList();
            originalReminders = ReminderMapper.MapChangesForUpdate(originalReminders, ToBeUpdatedReminders);

            _context.UpdateRange(originalReminders);
            _context.SaveChanges();
            return ReminderMapper.MapToBOList(originalReminders);
        }
    }
}
