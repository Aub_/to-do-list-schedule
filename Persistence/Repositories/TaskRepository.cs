﻿using Contracts;
using Domain;
using Domain.Repositories.Abstractions;
using Persistence.Entities;
using Persistence.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Persistence.Repositories
{
    internal class TaskRepository : ITaskRepository
    {
        private readonly TDLDBContext _context;
        public TaskRepository(TDLDBContext context)
        {
            _context = context;
        }

        public List<TaskBO> Add(List<TaskBO> newTasks)
        {
            List<Task> tasks = TaskMapper.MapToDAOList(newTasks);

            _context.Tasks.AddRange(tasks);

            _context.SaveChanges();
            return TaskMapper.MapToBOList(tasks);
        }

        public void Delete(List<int> TaskIds)
        {
            List<Task> tasks = _context.Tasks
                .Where(t => TaskIds.Contains(t.Id) && t.DeletedAt == null).ToList();

            foreach (Task task in tasks)
            {
                task.DeletedAt = DateTime.Now;
                task.StateId = (int)States.Cancelled;
            }
             _context.SaveChanges();
        }

        public void DeleteByUserId(int userId)
        {
            List<Task> tasks = _context.Tasks.Where(t => t.UserId == userId).ToList();
            foreach (Task task in tasks)
            {
                task.DeletedAt = DateTime.Now;
                task.StateId = (int)States.Cancelled;
            }
            _context.SaveChanges();
        }

        public List<TaskBO> FilterUserTasks(TaskFilter filter)
        {
            var query = _context.Tasks.Where(t => t.UserId == filter.UserID && 
                t.DeletedAt == null);

            if (filter.StartDate != null && filter.EndDate != null)
                query = query.Where(t => t.DateAndTime >= filter.StartDate && t.DateAndTime <= filter.EndDate);

            if (filter.Status != null) {
                int status_id = 0;
                if (filter.Status == "new") status_id = (int)States.New;
                else status_id = filter.Status == "done"? (int)States.Done : (int)States.Cancelled;
                query = query.Where(t => t.StateId == status_id);
                    }

            if (filter.Priority != null)
            {
                int priority_id = 0;
                if (filter.Priority == "high") priority_id = (int)Priorities.High;
                else priority_id = filter.Priority == "medium" ? (int)Priorities.Medium : (int)Priorities.Low;
                query = query.Where(t => t.PriorityId == priority_id);
            }

            List<Task> filteredTasks = query.ToList();
            return filteredTasks.MapToBOList();
        }

        public List<TaskBO> GetAll(bool withDeleted)
        {
            List<Task> tasks = new List<Task>();
            if (withDeleted)
                tasks = _context.Tasks.ToList();
            else tasks = _context.Tasks.Where(t => t.DeletedAt == null).ToList();

            return tasks.MapToBOList();
        }

        public TaskBO GetById(int id)
        {
            Task task = _context.Tasks.Where(t=>t.Id==id && t.DeletedAt == null).FirstOrDefault();
            if (task is null) return null;
            return task.MapToBO();
        }

        public List<TaskBO> GetByIds(List<int> ids)
        {
            List<Task> tasks = _context.Tasks.Where(t => ids.Contains(t.Id) && t.DeletedAt == null).ToList();
            return tasks.MapToBOList();
        }

        public List<TaskBO> GetByUserId(int id)
        {
            List<Task> tasks = _context.Tasks.Where(t => t.UserId == id).ToList();
            if (tasks is null || tasks.Count ==0) return null;
            return tasks.MapToBOList();
        }

        public bool TasksExist(List<int> ids)
        {
            List<int> DBTasks = _context.Tasks.Where(t=>t.DeletedAt == null).Select(t => t.Id).ToList();
            return ids.Except(DBTasks).Count()==0;
        }

        public List<TaskBO> Update(List<int> originalTaskIds,List<TaskBO> toBeUpdatedTasks)
        {
            List<Task> tasks = _context.Tasks
                .Where(t => originalTaskIds.Contains(t.Id) && t.DeletedAt == null).ToList();
            tasks = TaskMapper.MapChangesForUpdate(toBeUpdatedTasks,tasks);

            //making sure that the parent tasks (if any) exist.
            List<int> parentIds = toBeUpdatedTasks.Where(t => t.ParentId > 0)
                .Select(t => t.ParentId).Cast<int>().ToList();

                if (parentIds.Count() > 0 && !TasksExist(parentIds))
                return null;

            _context.UpdateRange(tasks);
            _context.SaveChanges();
            return tasks.MapToBOList();
        }
    }

    public enum States
    {
        Done=1,
        New=2,
        Cancelled=3
    }

    public enum Priorities
    {
        High = 1,
        Medium = 2,
        Low = 3
    }
}
