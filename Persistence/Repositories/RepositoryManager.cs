﻿using Domain;
using Domain.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Repositories
{
    public class RepositoryManager : IRepositoryManager
    {
        private readonly Lazy<ITaskRepository> _lazyTaskRepository;
        private readonly Lazy<IReminderRepository> _lazyReminderRepository;
        public RepositoryManager(TDLDBContext context)
        {
            _lazyTaskRepository = new Lazy<ITaskRepository>(() => new TaskRepository(context));
            _lazyReminderRepository = new Lazy<IReminderRepository>(() => new ReminderRepository(context));
        }

        public ITaskRepository TaskRepository => _lazyTaskRepository.Value;

        public IReminderRepository ReminderRepository => _lazyReminderRepository.Value;
    }
}
