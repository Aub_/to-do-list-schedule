﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class adddateandtimecolumntoreminderstableandenddatetotaskstable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "EndDate",
                table: "tasks",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "reminders",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(60)",
                oldMaxLength: 60,
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Time",
                table: "reminders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 1,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 22, 17, 15, 10, 864, DateTimeKind.Local).AddTicks(6979), new DateTime(2022, 6, 22, 17, 15, 10, 870, DateTimeKind.Local).AddTicks(8198) });

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 2,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 22, 17, 15, 10, 870, DateTimeKind.Local).AddTicks(9099), new DateTime(2022, 6, 22, 17, 15, 10, 870, DateTimeKind.Local).AddTicks(9136) });

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 3,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 22, 17, 15, 10, 870, DateTimeKind.Local).AddTicks(9158), new DateTime(2022, 6, 22, 17, 15, 10, 870, DateTimeKind.Local).AddTicks(9166) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 1,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 22, 17, 15, 10, 872, DateTimeKind.Local).AddTicks(5533), new DateTime(2022, 6, 22, 17, 15, 10, 872, DateTimeKind.Local).AddTicks(6004) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 2,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 22, 17, 15, 10, 872, DateTimeKind.Local).AddTicks(6460), new DateTime(2022, 6, 22, 17, 15, 10, 872, DateTimeKind.Local).AddTicks(6492) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 3,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 22, 17, 15, 10, 872, DateTimeKind.Local).AddTicks(6509), new DateTime(2022, 6, 22, 17, 15, 10, 872, DateTimeKind.Local).AddTicks(6514) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndDate",
                table: "tasks");

            migrationBuilder.DropColumn(
                name: "Time",
                table: "reminders");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "reminders",
                type: "nvarchar(60)",
                maxLength: 60,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 1,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 18, 27, 26, 444, DateTimeKind.Local).AddTicks(8698), new DateTime(2022, 6, 20, 18, 27, 26, 447, DateTimeKind.Local).AddTicks(8067) });

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 2,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 18, 27, 26, 447, DateTimeKind.Local).AddTicks(8652), new DateTime(2022, 6, 20, 18, 27, 26, 447, DateTimeKind.Local).AddTicks(8677) });

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 3,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 18, 27, 26, 447, DateTimeKind.Local).AddTicks(8690), new DateTime(2022, 6, 20, 18, 27, 26, 447, DateTimeKind.Local).AddTicks(8694) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 1,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 18, 27, 26, 449, DateTimeKind.Local).AddTicks(2353), new DateTime(2022, 6, 20, 18, 27, 26, 449, DateTimeKind.Local).AddTicks(2816) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 2,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 18, 27, 26, 449, DateTimeKind.Local).AddTicks(3267), new DateTime(2022, 6, 20, 18, 27, 26, 449, DateTimeKind.Local).AddTicks(3300) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 3,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 18, 27, 26, 449, DateTimeKind.Local).AddTicks(3315), new DateTime(2022, 6, 20, 18, 27, 26, 449, DateTimeKind.Local).AddTicks(3320) });
        }
    }
}
