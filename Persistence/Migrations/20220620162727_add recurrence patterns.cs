﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class addrecurrencepatterns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "recurrence_patterns",
                columns: table => new
                {
                    TaskId = table.Column<int>(type: "int", nullable: false),
                    SeparationCount = table.Column<int>(type: "int", nullable: false),
                    MaxNumOccurences = table.Column<int>(type: "int", nullable: false),
                    DayOfWeek = table.Column<int>(type: "int", nullable: false),
                    WeekOfMonth = table.Column<int>(type: "int", nullable: false),
                    DayOfMonth = table.Column<int>(type: "int", nullable: false),
                    MonthOfYear = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_recurrence_patterns", x => x.TaskId);
                    table.ForeignKey(
                        name: "FK_recurrence_patterns_tasks_TaskId",
                        column: x => x.TaskId,
                        principalTable: "tasks",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 1,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 18, 27, 26, 444, DateTimeKind.Local).AddTicks(8698), new DateTime(2022, 6, 20, 18, 27, 26, 447, DateTimeKind.Local).AddTicks(8067) });

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 2,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 18, 27, 26, 447, DateTimeKind.Local).AddTicks(8652), new DateTime(2022, 6, 20, 18, 27, 26, 447, DateTimeKind.Local).AddTicks(8677) });

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 3,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 18, 27, 26, 447, DateTimeKind.Local).AddTicks(8690), new DateTime(2022, 6, 20, 18, 27, 26, 447, DateTimeKind.Local).AddTicks(8694) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 1,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 18, 27, 26, 449, DateTimeKind.Local).AddTicks(2353), new DateTime(2022, 6, 20, 18, 27, 26, 449, DateTimeKind.Local).AddTicks(2816) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 2,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 18, 27, 26, 449, DateTimeKind.Local).AddTicks(3267), new DateTime(2022, 6, 20, 18, 27, 26, 449, DateTimeKind.Local).AddTicks(3300) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 3,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 18, 27, 26, 449, DateTimeKind.Local).AddTicks(3315), new DateTime(2022, 6, 20, 18, 27, 26, 449, DateTimeKind.Local).AddTicks(3320) });

            migrationBuilder.CreateIndex(
                name: "IX_reminders_TaskID",
                table: "reminders",
                column: "TaskID");

            migrationBuilder.AddForeignKey(
                name: "FK_reminders_tasks_TaskID",
                table: "reminders",
                column: "TaskID",
                principalTable: "tasks",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_reminders_tasks_TaskID",
                table: "reminders");

            migrationBuilder.DropTable(
                name: "recurrence_patterns");

            migrationBuilder.DropIndex(
                name: "IX_reminders_TaskID",
                table: "reminders");

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 1,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 16, 42, 52, 170, DateTimeKind.Local).AddTicks(7447), new DateTime(2022, 6, 20, 16, 42, 52, 174, DateTimeKind.Local).AddTicks(366) });

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 2,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 16, 42, 52, 174, DateTimeKind.Local).AddTicks(1461), new DateTime(2022, 6, 20, 16, 42, 52, 174, DateTimeKind.Local).AddTicks(1491) });

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 3,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 16, 42, 52, 174, DateTimeKind.Local).AddTicks(1503), new DateTime(2022, 6, 20, 16, 42, 52, 174, DateTimeKind.Local).AddTicks(1508) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 1,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 16, 42, 52, 176, DateTimeKind.Local).AddTicks(177), new DateTime(2022, 6, 20, 16, 42, 52, 176, DateTimeKind.Local).AddTicks(708) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 2,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 16, 42, 52, 176, DateTimeKind.Local).AddTicks(1218), new DateTime(2022, 6, 20, 16, 42, 52, 176, DateTimeKind.Local).AddTicks(1251) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 3,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 16, 42, 52, 176, DateTimeKind.Local).AddTicks(1266), new DateTime(2022, 6, 20, 16, 42, 52, 176, DateTimeKind.Local).AddTicks(1272) });
        }
    }
}
