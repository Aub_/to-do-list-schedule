﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class addseeds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "task_priorities",
                columns: new[] { "ID", "CreatedAt", "DeletedAt", "Name", "UpdatedAt" },
                values: new object[,]
                {
                    { 1, new DateTime(2022, 6, 2, 20, 48, 1, 818, DateTimeKind.Local).AddTicks(581), null, "High", new DateTime(2022, 6, 2, 20, 48, 1, 823, DateTimeKind.Local).AddTicks(3884) },
                    { 2, new DateTime(2022, 6, 2, 20, 48, 1, 823, DateTimeKind.Local).AddTicks(4860), null, "Medium", new DateTime(2022, 6, 2, 20, 48, 1, 823, DateTimeKind.Local).AddTicks(4904) },
                    { 3, new DateTime(2022, 6, 2, 20, 48, 1, 823, DateTimeKind.Local).AddTicks(4931), null, "Low", new DateTime(2022, 6, 2, 20, 48, 1, 823, DateTimeKind.Local).AddTicks(4939) }
                });

            migrationBuilder.InsertData(
                table: "task_states",
                columns: new[] { "ID", "CreatedAt", "DeletedAt", "Name", "UpdatedAt" },
                values: new object[,]
                {
                    { 1, new DateTime(2022, 6, 2, 20, 48, 1, 826, DateTimeKind.Local).AddTicks(930), null, "New", new DateTime(2022, 6, 2, 20, 48, 1, 826, DateTimeKind.Local).AddTicks(1749) },
                    { 2, new DateTime(2022, 6, 2, 20, 48, 1, 826, DateTimeKind.Local).AddTicks(2654), null, "Done", new DateTime(2022, 6, 2, 20, 48, 1, 826, DateTimeKind.Local).AddTicks(2708) },
                    { 3, new DateTime(2022, 6, 2, 20, 48, 1, 826, DateTimeKind.Local).AddTicks(2735), null, "Cancelled", new DateTime(2022, 6, 2, 20, 48, 1, 826, DateTimeKind.Local).AddTicks(2744) }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 3);
        }
    }
}
