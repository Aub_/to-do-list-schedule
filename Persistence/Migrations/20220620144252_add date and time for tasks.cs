﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class adddateandtimefortasks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateAndTime",
                table: "tasks",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 1,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 16, 42, 52, 170, DateTimeKind.Local).AddTicks(7447), new DateTime(2022, 6, 20, 16, 42, 52, 174, DateTimeKind.Local).AddTicks(366) });

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 2,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 16, 42, 52, 174, DateTimeKind.Local).AddTicks(1461), new DateTime(2022, 6, 20, 16, 42, 52, 174, DateTimeKind.Local).AddTicks(1491) });

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 3,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 16, 42, 52, 174, DateTimeKind.Local).AddTicks(1503), new DateTime(2022, 6, 20, 16, 42, 52, 174, DateTimeKind.Local).AddTicks(1508) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 1,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 16, 42, 52, 176, DateTimeKind.Local).AddTicks(177), new DateTime(2022, 6, 20, 16, 42, 52, 176, DateTimeKind.Local).AddTicks(708) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 2,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 16, 42, 52, 176, DateTimeKind.Local).AddTicks(1218), new DateTime(2022, 6, 20, 16, 42, 52, 176, DateTimeKind.Local).AddTicks(1251) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 3,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 20, 16, 42, 52, 176, DateTimeKind.Local).AddTicks(1266), new DateTime(2022, 6, 20, 16, 42, 52, 176, DateTimeKind.Local).AddTicks(1272) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateAndTime",
                table: "tasks");

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 1,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 2, 20, 48, 1, 818, DateTimeKind.Local).AddTicks(581), new DateTime(2022, 6, 2, 20, 48, 1, 823, DateTimeKind.Local).AddTicks(3884) });

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 2,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 2, 20, 48, 1, 823, DateTimeKind.Local).AddTicks(4860), new DateTime(2022, 6, 2, 20, 48, 1, 823, DateTimeKind.Local).AddTicks(4904) });

            migrationBuilder.UpdateData(
                table: "task_priorities",
                keyColumn: "ID",
                keyValue: 3,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 2, 20, 48, 1, 823, DateTimeKind.Local).AddTicks(4931), new DateTime(2022, 6, 2, 20, 48, 1, 823, DateTimeKind.Local).AddTicks(4939) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 1,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 2, 20, 48, 1, 826, DateTimeKind.Local).AddTicks(930), new DateTime(2022, 6, 2, 20, 48, 1, 826, DateTimeKind.Local).AddTicks(1749) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 2,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 2, 20, 48, 1, 826, DateTimeKind.Local).AddTicks(2654), new DateTime(2022, 6, 2, 20, 48, 1, 826, DateTimeKind.Local).AddTicks(2708) });

            migrationBuilder.UpdateData(
                table: "task_states",
                keyColumn: "ID",
                keyValue: 3,
                columns: new[] { "CreatedAt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 6, 2, 20, 48, 1, 826, DateTimeKind.Local).AddTicks(2735), new DateTime(2022, 6, 2, 20, 48, 1, 826, DateTimeKind.Local).AddTicks(2744) });
        }
    }
}
