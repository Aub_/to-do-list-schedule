﻿using System;
namespace Contracts
{
    public class TaskBO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Time { get; set; }
        public int? ParentId { get; set; }
        public int? PriorityId { get; set; }
        public int? StateId { get; set; }
        public bool? IsRecurring { get; set; }
        public int UserId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

    }
}