﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public class ReminderBO
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public string Description { get; set; }
        public int TaskId { get; set; }
        public bool? IsDisabled { get; set; }
        public bool? IsDone { get; set; }

    }
}
