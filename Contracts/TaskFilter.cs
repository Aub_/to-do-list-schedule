﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public class TaskFilter
    {
        public int UserID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public string Priority { get; set; }
    }
}
