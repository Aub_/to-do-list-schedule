﻿using Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Abstractions
{
    public interface ITaskService
    {
        public TaskBO GetById(int id);
        public List<TaskBO> GetAll();
        public List<TaskBO> GetByUserId(int id);
        public List<TaskBO> FilterUserTasks(TaskFilter filter);
        public List<TaskBO> Add(List<TaskBO> newTasks);
        public List<TaskBO> Update(List<TaskBO> toBeUpdatedTasks);
        public bool Delete(List<int> toBeDeletedTasks);
        public bool DeleteByUserId(int userId);
    }
}
