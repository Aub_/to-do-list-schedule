﻿using Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Abstractions
{
    public interface IReminderService
    {
        public List<ReminderBO> GetByTaskID(int TaskId);
        public ReminderBO GetById(int Id);
        public List<ReminderBO> GetByIds(List<int> Ids);
        public List<ReminderBO> GetOngingTaskReminders(int TaskId);
        public List<ReminderBO> Add(List<ReminderBO> newReminders, int TaskId);
        public List<ReminderBO> Update(List<int> OriginalRemindersIds, List<ReminderBO> ToBeUpdatedReminders);
        public void Delete(List<int> toBeDeletedTasks);
        public void DeleteByTaskId(int taskId);
        public void DisableBytTaskId(int taskId);
        public bool RemindersExist(List<int> ids);
    }
}
