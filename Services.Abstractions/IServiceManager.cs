﻿namespace Services.Abstractions
{
    public interface IServiceManager
    {
        public ITaskService TaskService { get; }
        public IReminderService ReminderService { get; }
    }
}