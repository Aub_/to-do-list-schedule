﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Repositories.Abstractions
{
    public interface IRepositoryManager
    {
        public ITaskRepository TaskRepository { get; }
        public IReminderRepository ReminderRepository { get; }
    }
}
