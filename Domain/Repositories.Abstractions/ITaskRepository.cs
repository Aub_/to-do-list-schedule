﻿using Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Repositories.Abstractions
{
    public interface ITaskRepository
    {
        public TaskBO GetById(int id);
        public List<TaskBO> GetAll(bool withDeleted=false);
        public List<TaskBO> GetByIds(List<int> ids);
        public List<TaskBO> GetByUserId(int id);
        public List<TaskBO> FilterUserTasks(TaskFilter filter);
        public List<TaskBO> Add(List<TaskBO> newTasks);
        public List<TaskBO> Update(List<int> originalTaskIds,List<TaskBO> toBeUpdatedTasks);
        public bool TasksExist(List<int> ids);
        public void Delete(List<int> toBeDeletedTasks);
        public void DeleteByUserId(int userId);
    }
}
